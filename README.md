# Saipos-api

Api para ser consumido pelo SPA Saipos

Como iniciar:

1. Crie o banco de dados no servidor MySql com o dump estrutural que se encontra no arquivo 'saipos.sql' na pasta raiz do projeto.
2. Acesse database/connection.js a partir da raiz e configure a conexão com o banco.
3. Dentro da pasta raiz, abra o prompt de comando e rode 'npm install' para instalar as dependências.
4. Em seguida, rode 'npm start' para a api ser iniciada.
5. A api ficará acessível em http://localhost:3000.
