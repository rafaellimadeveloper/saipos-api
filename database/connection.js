var mysql      = require('mysql');

const create_con = async () => {
    return mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'saipos' 
    })
}
module.exports.con = create_con;