require('dotenv').config()
var express = require('express');
const axios = require('axios')
var router  = express.Router();
var con     = require('../database/connection')
var email   = require('../helpers/email')
var bcrypt  = require('bcrypt')

//PRO TESTE EM SI NÃO VI NECESSIDADE, MAS PARA PROJETOS MAIORES EU UTILIZARIA CONTROLLERS
router.post('/', async function(req, res, next) {
  const {description, name_keeper, email_keeper} = req.body
  
  const valid_email = await email.validate(email_keeper)
  if(!valid_email){
    res.json({success: false, message: "Ops! Tivemos um problema de comunicação. Tente novamente."})
  }else if(valid_email && !valid_email.success){
    var message = !valid_email.did_you_mean ? "Email inválido!" : `Email inválido! Será que você não quis dizer: ${valid_email.did_you_mean}`
    res.json({success: false, message})
  }else{
    const connection = await con.con()
    connection.connect()
    var sql = `INSERT INTO task(description, name_keeper, email_keeper, status) 
               VALUES('${description}', '${name_keeper}', '${email_keeper}', 'pending')`
    await connection.query(sql, function (err, result) {
      if (err){
        res.json({success: false, message:"Erro ao salvar! Verifique se as informações foram preenchidas corretamente"})
      }else{
        res.json({success: true, message:"Atividade criada com sucesso!"})
      }
      connection.end() 
    });
  }
});

router.post('/done-task', async function(req, res, next) {
  const task = req.body
  const connection = await con.con()
  await connection.connect()
  const sql = `UPDATE task set status = 'done' WHERE id = ${task.id}`
  connection.query(sql, function (err, result, fields) {
    connection.end() 
    if (err) throw err;
    res.json({success: true, message: 'Tarefa concluída com sucesso'})
  });
});

router.post('/pending-task', async function(req, res, next) {
  var task = req.body.task
  const password = req.body.password
  
  if(await bcrypt.compare(password, process.env.password)){
  
    const connection = await con.con()
    await connection.connect()

    var sql = `SELECT * FROM task WHERE id = ${task.id} limit 1`
    
    connection.query(sql,async function (err, result, fields) {
      if (err) throw err;
      task = result[0]
    
      if(task.status_rollback < process.env.limit_status_rollback){
      
        sql = `UPDATE task 
              SET status = 'pending',status_rollback = coalesce(status_rollback, 0)+1 
              WHERE id = ${task.id}`
        connection.query(sql, function (err, result, fields) {
          connection.end() 
          if (err) throw err;
          res.json({success: true, message: 'Tarefa movida como pendente com sucesso'})
        });
      }else{
        res.json({success: false, message: `Essa atividade ultrapassou o limite de ${task.status_rollback} vez(es) que ela pode voltar como pendente`})
      }
    })
  }else{
    res.json({success: false, message: 'Senha incorreta!'})
  }
});
router.get('/list-tasks', async function(req, res, next) {
  const connection = await con.con()
  await connection.connect()
  var sql = "SELECT * FROM task"
  
  connection.query(sql, function (err, result, fields) {
    connection.end() 
    if (err) throw err;
    res.json({success: true, tasks: result})
  });
});

router.get('/random-tasks', async function(req, res, next) {
  //A API PARA CONSULTA DE GATOS PARECIA ESTAR FORA DO AR, ACHEI UMA DE CACHORROS PARA SUBSTITUIR 
  const response = await axios.get(`https://dog-facts-api.herokuapp.com/api/v1/resources/dogs?number=3`)
  const connection = await con.con()
  await connection.connect()
  var sql = 'INSERT INTO task(description, name_keeper, email_keeper, status) VALUES'
  
  //ISSO AQUI TA MUITO FEIO, KKKK
  response.data.forEach(function(item, index) {
    sql += `('${item.fact}', 'Eu', 'eu@me.com', 'pending')`
    if(index+1 < response.data.length)
      sql += ','
  }); 
             
  await connection.query(sql, function (err, result) {
    if (err){
      res.json({success: false, message:"Erro ao gerar! Informe ao administrador do sistema"})
    }else{
      res.json({success: true, message:"Atividades criadas com sucesso!"})
    }
    connection.end() 
  });
});

module.exports = router;
