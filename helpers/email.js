const axios = require('axios')
require('dotenv').config()

const validate = async function(email) {
    try{
        const response = await axios.get(`http://apilayer.net/api/check?access_key=${process.env.mail_box_layer_key}&email=${email}`)
        const {did_you_mean, format_valid, mx_found} = response.data
        return !format_valid || !mx_found ? { success: false, did_you_mean } : { success: true }
    }catch(error) {
        return false
    }
}

exports.validate = validate 